import * as Phaser from 'phaser'

import { BootScene } from '../scenes/boot'
import { GameScene } from '../scenes/game'
import { PhaserTestScene } from '../scenes/test/phaser-test'
import { GameWorld } from './game-world'

export const GameUrl: string = 'http://127.0.0.1:4000'

export const GameConfig: Phaser.Types.Core.GameConfig = {
  type: Phaser.WEBGL,
  scene: [GameScene],
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'game',
    width: GameWorld.WORLD_WIDTH,
    height: GameWorld.WORLD_HEIGHT,
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 200 },
      debug: true,
    },
  },
  render: {
    pixelArt: true,
  },
}
