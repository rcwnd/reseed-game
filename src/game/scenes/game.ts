import * as Phaser from 'phaser'

export class GameScene extends Phaser.Scene {
  protected player: Phaser.Physics.Arcade.Sprite

  /**
   * Use it to create your game objects. This method is called by the Scene
   * Manager when the scene starts, after init() and preload()
   */
  public create() {
    const platforms = this.physics.add.staticGroup()
    platforms
      .create(this.scale.width / 2.0, this.scale.height / 2.0, 'ground')
      .setScale(2)
      .refreshBody()
    this.player = this.physics.add.sprite(100, 450, 'dude')
    this.player.setBounce(0.2)
    this.player.setCollideWorldBounds(true)

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
      frameRate: 10,
      repeat: -1,
    })

    this.anims.create({
      key: 'turn',
      frames: [{ key: 'dude', frame: 4 }],
      frameRate: 20,
    })

    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
      frameRate: 10,
      repeat: -1,
    })
    this.physics.add.collider(this.player, platforms)
    console.log('hej')
  }

  /**
   * Use it to load assets. This method is called by the Scene Manager,
   * after init() and before create(), only if the Scene has a LoaderPlugin
   */
  public preload() {
    console.log('loading')
    this.load.setBaseURL('http://labs.phaser.io')
    this.load.image('orb-blue', 'assets/sprites/orb-blue.png')
    this.load.image('orb-red', 'assets/sprites/orb-red.png')
    this.load.image('orb-green', 'assets/sprites/orb-green.png')
    this.load.spritesheet('dude', 'src/games/firstgame/assets/dude.png', {
      frameWidth: 32,
      frameHeight: 48,
    })
    this.load.image('ground', 'src/games/firstgame/assets/platform.png')
  }
}
