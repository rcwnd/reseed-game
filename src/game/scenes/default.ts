import * as Phaser from 'phaser'

export class DefaultScene extends Phaser.Scene {
  constructor(key: string) {
    super({ key })
  }

  /**
   * Use it to create your game objects. This method is called by the Scene
   * Manager when the scene starts, after init() and preload()
   */
  public create() {
    return
  }

  /**
   * This method is called by the Scene Manager when the scene starts, before preload() and create()
   */
  public init() {
    return
  }

  /**
   * Use it to load assets. This method is called by the Scene Manager,
   * after init() and before create(), only if the Scene has a LoaderPlugin
   */
  public preload() {
    return
  }
}
